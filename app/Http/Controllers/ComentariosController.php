<?php

namespace App\Http\Controllers;

use App\Models\Comentario;
use Illuminate\Http\Request;

class ComentariosController extends Controller
{

    public function index()
    {
        return Comentario::orderBy('created_at', 'desc')->paginate();
    }

    public function post(Request $request)
    {
        $comentario = Comentario::create([
            'nome' => $request->get('nome'),
            'comentario' => $request->get('comentario')
        ]);

        return response()->json($comentario);
    }

    public function put(Request $request, $id)
    {
        $comentario = Comentario::find($id);

        $comentario->nome = $request->get('nome');
        $comentario->comentario = $request->get('comentario');

        $comentario->save();

        return response()->json($comentario);
    }

    public function delete($id)
    {
        return response()->json(Comentario::destroy($id));
    }
}