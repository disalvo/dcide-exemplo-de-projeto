<?php

$app->get('/', function () use ($app) {
    return view('home');
});

$app->group(['prefix' => 'api/v1'], function ($app) {
    $app->get('comentarios', 'App\Http\Controllers\ComentariosController@index');

    $app->post('comentarios', 'App\Http\Controllers\ComentariosController@post');

    $app->put('comentarios/{id}', 'App\Http\Controllers\ComentariosController@put');

    $app->delete('comentarios/{id}', 'App\Http\Controllers\ComentariosController@delete');
});
