<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Comentario extends Model {

    protected $table = 'comentarios';

    protected $fillable = [
        'nome',
        'comentario'
    ];

    public function getCreatedAtAttribute($value)
    {
        return (new \DateTime($value))->format('d/m/Y H:i:s');
    }

    public function getUpdatedAtAttribute($value)
    {
        return (new \DateTime($value))->format('d/m/Y H:i:s');
    }
}