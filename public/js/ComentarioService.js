app.service('ComentarioService', function($resource) {
    return $resource('/api/v1/comentarios/:id', {}, {
        query: {
            method: 'GET',
            isArray: false,
            params: {
                page: '@page',
                count: '@count',
                filter: '@filter'
            }
        },
        create: {
            method: 'POST'
        },
        show: {
            method: 'GET'
        },
        update: {
            method: 'PUT',
            params: {
                id: '@id'
            }
        },
        delete: {
            method: 'DELETE',
            params: {
                id: '@id'
            }
        }
    })
});