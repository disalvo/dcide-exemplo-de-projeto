app.controller('ComentariosController', function($scope, $http, ComentarioService) {
    $scope.comentarios = [];
    $scope.loading = false;

    $scope.listaComentarios = function() {
        ComentarioService.query().$promise.then(function(response) {
            $scope.comentarios = response.data;
        });
    };

    $scope.initComentario = function() {
        $scope.comentario = {
            nome: "",
            comentario: ""
        };
    };

    $scope.initComentario();
    $scope.listaComentarios();

    $scope.inserir = function() {
        $scope.loading = true;
        if($scope.comentario.id == undefined) {
            ComentarioService
                .create($scope.comentario)
                .$promise
                .then(
                    function(response) {
                        $scope.loading = false;
                        $scope.listaComentarios();
                        $scope.initComentario();
                    },
                    function(response) {}
                );
        } else {
            ComentarioService
                .update($scope.comentario)
                .$promise
                .then(
                    function(response) {
                        $scope.loading = false;
                        $scope.listaComentarios();
                        $scope.initComentario();
                    },
                    function(response) {}
                );
        }
    };

    $scope.editar = function(c) {
        var obj = angular.copy(c);
        $scope.comentario = obj;
    };

    $scope.excluir = function(id) {
        ComentarioService
            .delete({id: id})
            .$promise
            .then(
                function(response) {
                    $scope.listaComentarios();
                },
                function(response) {}
            );
    };

});