## Dcide - Exemplo de projeto

Projeto CRUD de exemplo, desenvolvido com as seguintes tecnologias:

* [Lumen ](https://lumen.laravel.com/docs/5.1/installation)(micro framework PHP)
* AngularJS
* Bootstrap

Integração via JSON entre front-end e back-end.

## Instalação

1. Clonar o projeto;
2. Para criar a base de dados, utilizar o dump, localizado em: [database/dump.sql](https://bitbucket.org/disalvo/dcide-exemplo-de-projeto/src/a69b62566e3610b2a4523144a8e9bd1055dfba96/database/dump.sql?at=master&fileviewer=file-view-default);
3. As configurações de conexão com o banco de dados, se encontram no arquivo [.env](https://bitbucket.org/disalvo/dcide-exemplo-de-projeto/src/a69b62566e3610b2a4523144a8e9bd1055dfba96/.env?fileviewer=file-view-default);
4. Agora é só executar!

## Executando

No terminal Linux (ou prompt de comando Windows):

$> php artisan serve

Abrir o browser no endereço informado no comando acima.