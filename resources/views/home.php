<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="Exemplo de aplicação CRUD de comentários para Dcide">
    <meta name="author" content="Alexandre Di Salvo">
    <link rel="icon" href="../../favicon.ico">

    <title>Dcide - Exemplo Comentários</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body ng-app="app" ng-controller="ComentariosController">

<div class="container">
    <div class="page-header">
        <h1>Comentários</h1>
    </div>

    <div class="col-md-12">
        <div class="col-md-4">
            <form name="form" novalidate>
                <div class="form-group" ng-class="{ 'has-error': form.nome.$touched && form.nome.$invalid }">
                    <label for="nome">Nome</label>
                    <input class="form-control" name="nome" id="nome" required ng-model="comentario.nome" />
                    <div ng-messages="form.nome.$error" ng-if="form.nome.$touched" class="text-danger" role="alert">
                        <div ng-message="required">Campo obrigatório</div>
                    </div>
                </div>
                <div class="form-group" ng-class="{ 'has-error': form.nome.$touched && form.nome.$invalid }">
                    <label for="comentario">Comentário</label>
                    <textarea class="form-control" name="comentario" id="comentario" cols="30" rows="10" required ng-model="comentario.comentario"></textarea>
                    <div ng-messages="form.comentario.$error" ng-if="form.comentario.$touched" class="text-danger" role="alert">
                        <div ng-message="required">Campo obrigatório</div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success" ng-show="!loading" ng-disabled="form.$invalid" ng-click="inserir()" ng-bind="(comentario.id == undefined) ? 'Inserir' : 'Editar'"></button>
                    <button type="button" class="btn btn-success" ng-show="loading" disabled>Inserindo..</button>
                </div>
            </form>
        </div>
        <div class="col-md-8">
            <div class="media" ng-repeat="c in comentarios">
                <div class="media-body">
                    <h4 class="media-heading">
                        <span ng-bind="c.nome"></span>
                        <small ng-bind="c.updated_at"></small>
                    </h4>
                    <span ng-bind="c.comentario"></span>
                </div>
                <button class="btn btn-sm btn-primary" ng-click="editar(c)">Editar</button>
                <button class="btn btn-sm btn-danger" ng-click="excluir(c.id)">Excluir</button>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-route.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-resource.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular-messages.min.js"></script>

<script src="js/app.js"></script>
<script src="js/ComentarioService.js"></script>
<script src="js/Comentarios.js"></script>

</body>
</html>
